from jinja2 import Environment, FileSystemLoader, select_autoescape

def generate_partner_config(domain_name: str, config_template_path: str,  output_file: str):
    """
    Fills out a grafana.ini.template at the given template path for a partner instance, and creates new file at given output file path.

    :param domain_name: internet domain name where grafana will be running
    :param config_template_path: folder that contains a grafana.ini.template file.
    :param output_file: grafana ini file that will be created
    :return:
    """
    output = _build_config(domain_name, config_template_path, enable_anonymous_auth=False)

    with open(output_file, 'w') as f:
        f.write(output)

def generate_central_config(domain_name: str, config_template_path: str,  output_file: str):
    """
    Fills out a grafana.ini.template at the given template path for a central instance, and creates new file at given output file path.

    :param domain_name: internet domain name where grafana will be running
    :param config_template_path: folder that contains a grafana.ini.template file.
    :param output_file: grafana ini file that will be created
    :return:
    """
    output = _build_config(domain_name, config_template_path, enable_anonymous_auth=True)

    with open(output_file, 'w') as f:
        f.write(output)

def _build_config(domain_name: str, template_path: str, enable_anonymous_auth: bool) -> str:
    """
    Applies jinja replacement in grafana.ini.template file at given template_path.

    :param domain_name: internet domain name where grafana will be running
    :param template_path: folder that contains a grafana.ini.template file.
    :param enable_anonymous_auth: flag to enable anonymous access
    :return: content of a grafana config file.
    """
    print(f"Building the config using templates at {template_path}")

    env = Environment(
        loader=FileSystemLoader(template_path),
        autoescape=select_autoescape(['yml', 'yaml'])
    )
    template = env.get_template("grafana.ini.template")
    output = template.render(domain_name=domain_name, enable_anonymous_auth=enable_anonymous_auth )
    return output
