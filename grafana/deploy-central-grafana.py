#!/usr/bin/env python3

import argparse
import subprocess
import os
from config_generator import generate_central_config
from dashboards import include_dashboard_in_deployment
from kubernetes_configs import create_central_ingress_config
from shutil import copytree
from typing import Any, Dict, List
from urllib.parse import urljoin
from vault_secrets import VaultSecrets

DRY_RUN = "dry_run"
DOMAIN_NAME = "domain_name"
VAULT_MONITORING_PATH="vault_monitoring_path"
VAULT_CERT_PATH="vault_cert_path"
OAUTH_CLIENT_ID="oauth_client_id"
OAUTH_CLIENT_SECRET = "oauth_client_secret"
OUTPUT_DIR = "output_dir"

K8S_PATH = "k8s"
BASE_K8S_PATH = "base"
CENTRAL_K8S_PATH = "central"
CONFIG_PATH = "config/"

GRAFANA_ADMIN_PASS_VAULT_KEY="admin_password"

def main():
    """The main function which will combine all the different steps of running the deployment.
    """
    args = get_args()
    dry_run = args[DRY_RUN]
    domain_name = args[DOMAIN_NAME]
    vault_secrets = VaultSecrets(args[VAULT_CERT_PATH])
    vault_base_path = args[VAULT_MONITORING_PATH]
    output_dir = args[OUTPUT_DIR]

    output_k8s_dir = os.path.join(output_dir, K8S_PATH)
    copytree(k8s_path(), output_k8s_dir)

    build_config(domain_name, output_k8s_dir)
    # Copy over the K8S files to the output since that will
    # create what we need for a deployment.
    include_dashboard_in_deployment(output_k8s_dir)

    get_admin_password_from_vault(output_k8s_dir, vault_secrets=vault_secrets, vault_base_path=vault_base_path)
    get_oauth_from_vault(output_k8s_dir, vault_secrets=vault_secrets, vault_base_path=vault_base_path)

    create_central_ingress_config(domain_name=domain_name, output_dir=output_k8s_dir)
    kubernetes_definitions_path = setup_deploy(output_k8s_dir, dry_run=dry_run)
    apply_kubernetes_definitions(kubernetes_definitions_path=kubernetes_definitions_path, dry_run=dry_run)


def get_args() -> Dict[str, Any]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """
    parser = argparse.ArgumentParser(description="Deploy a Grafana instance")

    # store_true says to default to false, but if specified would then turn in to true.
    parser.add_argument('--dry-run',
                        dest="dry_run",
                        action="store_true",
                        help="If true, don't actually deploy the application and just print what would happen on a deployment.")

    parser.add_argument("domain_name",
                        type=str,
                        metavar="DOMAIN_NAME",
                        help="The name of the domain that should resolve to this instance of grafana. (e.g. grafana.xand.tools)")

    parser.add_argument("vault_monitoring_path",
                        type=str,
                        metavar="VAULT_MONITORING_PATH",
                        help="The path in vault to the base directory of monitoring. (e.g. environments/dev/monitoring)")

    parser.add_argument("vault_cert_path",
                        type=str,
                        metavar="VAULT_CERT_PATH",
                        help="The path to the root certificate to be used with vault. (e.g. /etc/ssl/certs/devCA.pem)")

    parser.add_argument("output_dir",
                        type=str,
                        metavar="OUTPUT_DIR",
                        help="The output folder where the deployment artifacts will land.")

    args = parser.parse_args()
    return {
        DRY_RUN: args.dry_run,
        VAULT_MONITORING_PATH: args.vault_monitoring_path,
        DOMAIN_NAME: args.domain_name,
        VAULT_CERT_PATH: args.vault_cert_path,
        OUTPUT_DIR: args.output_dir
    }

def get_admin_password_from_vault(output_dir: str, vault_secrets: VaultSecrets, vault_base_path: str):
    """Creates the admin-password.env file that kustomize will expect to be there which will hold the values from vault.
    Doesn't need to print anything for a dry run since it's included in the kustomize output.

    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    :param vault_secrets: Instance of VaultSecrets where admin password is stored
    :type vault_secrets: VaultSecrets
    :param vault_base_path: base path to monitoring secrets in vault. e.g. environments/dev/monitoring
    :type vault_base_path: str
    """
    path = urljoin(vault_base_path, "grafana")
    admin_password_file_output = os.path.abspath(
        os.path.join(output_dir, BASE_K8S_PATH, "admin-password.env")
    )
    print(f"base path: {vault_base_path}")
    print(f"Retrieving grafana admin password from vault at {path} and storing it at: {admin_password_file_output}")
    admin_password = vault_secrets.get_from_vault(path, GRAFANA_ADMIN_PASS_VAULT_KEY)

    contents = f"GF_SECURITY_ADMIN_PASSWORD={admin_password}"
    contents += os.linesep

    with open(admin_password_file_output, 'w') as f:
        f.write(contents)


def get_oauth_from_vault(output_dir: str, vault_secrets: VaultSecrets, vault_base_path: str):
    """Creates the grafana-oauth-client.env file that kustomize will expect to be there which will hold the values from vault.
    Doesn't need to print anything for a dry run since it's included in the kustomize output.

    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    :param vault_secrets: Instance of VaultSecrets where admin password is stored
    :type vault_secrets: VaultSecrets
    :param vault_base_path: base path to monitoring secrets in vault. e.g. environments/dev/monitoring
    :type vault_base_path: str
    """
    path = urljoin(vault_base_path, "oauth")
    oauth_output_path = os.path.abspath(
        os.path.join(output_dir, CENTRAL_K8S_PATH, "grafana-oauth-client.env")
    )
    print(f"Retrieving oauth from vault at {path} and storing it at: {oauth_output_path}")
    oauth_client_id = vault_secrets.get_from_vault(path, OAUTH_CLIENT_ID)
    oauth_client_secret = vault_secrets.get_from_vault(path, OAUTH_CLIENT_SECRET)

    contents = f"client_id={oauth_client_id}"
    contents += os.linesep
    contents += f"client_secret={oauth_client_secret}"
    contents += os.linesep

    with open(oauth_output_path, 'w') as f:
        f.write(contents)

def build_config(domain_name: str, output_dir: str):
    """
    Builds a grafana.ini config file


    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    :param domain_name: Internet domain name where grafana instance will be running
    :type domain_name: str
    """
    config_template_path = os.path.abspath(
        os.path.join(os.getcwd(), CONFIG_PATH)
    )
    output_file = os.path.abspath(
        os.path.join(output_dir, CENTRAL_K8S_PATH,  "grafana.ini")
    )
    generate_central_config(domain_name, config_template_path, output_file)

def setup_deploy(output_dir: str, dry_run: bool) -> str:
    """Sets up the deployment by building the kustomize definition using what's in k8s/central.

    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    :param dry_run: Print what would happen on a deployment rather then deploying if true.
    :type dry_run: bool
    """
    output_file = os.path.abspath(
        os.path.join(output_dir, "grafana.yaml")
    )
    central_k8s_path = os.path.abspath(
        os.path.join(output_dir, CENTRAL_K8S_PATH)
    )

    print(f"Running kustomize at {central_k8s_path}")
    print(f"Setting up deployment by running kustomize and placing result at {output_file}")

    # This runs kustomize and outputs to central-monitoring.yaml
    kustomize_build_result = subprocess.run(["kustomize", "build", "."], cwd=central_k8s_path, capture_output=True, check=True, encoding="utf-8")
    contents = kustomize_build_result.stdout

    with open(output_file, "w") as f:
        f.write(contents)

    if dry_run:
        print("Kustomize Build output:")
        print(contents)

    return output_file

def k8s_path() -> str:
    return os.path.abspath(
        os.path.join(os.getcwd(), K8S_PATH)
    )

def apply_kubernetes_definitions(kubernetes_definitions_path: str, dry_run: bool):
    """Actually perform the apply of the kubernetes definitions built from kustomize.

    :param kubernetes_definitions_path: The path to the kubernetes definitions file that was generated by kustomize.
    :type kubernetes_definitions_path: str
    """

    print("Running kubernetes apply.")
    cmd = ["kubectl", "apply", "-f", kubernetes_definitions_path]
    if dry_run:
        cmd.append("--dry-run")
    subprocess.run(cmd, check=True, encoding="utf-8")

if __name__ == "__main__":
    main()
