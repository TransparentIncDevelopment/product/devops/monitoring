import os
from jinja2 import Environment, FileSystemLoader

BASE_K8S_PATH = "base"


def create_central_ingress_config(domain_name: str, output_dir: str):
    """Replaces values in the ingress template file and outputs it to a place where
    kustomize will pick it up. Doesn't need to print anything for a dry run since it's included
    in the kustomize output.

    :param domain_name: The domain name to be used with this deployment of the central server.
    :type domain_name: str
    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    """
    secret_name = domain_name.replace(".", "-")

    ingress_output_file = os.path.abspath(
        os.path.join(output_dir, BASE_K8S_PATH, "ingress.yaml")
    )

    data = _build_ingress_config(output_dir, domain_name, secret_name, True)

    with open(ingress_output_file, "w") as file:
        file.write(data)

def create_partner_ingress_config(domain_name: str, output_dir: str):
    """Replaces values in the ingress template file and outputs it to a place where
    kustomize will pick it up. Doesn't need to print anything for a dry run since it's included
    in the kustomize output.

    :param domain_name: The domain name to be used with this deployment of the central server.
    :type domain_name: str
    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    """
    secret_name = domain_name.replace(".", "-")

    ingress_output = os.path.abspath(
        os.path.join(output_dir, BASE_K8S_PATH, "ingress.yaml")
    )

    data = _build_ingress_config(output_dir, domain_name, secret_name, False)

    with open(ingress_output, "w") as file:
        file.write(data)

def _build_ingress_config(output_dir: str, url: str, secret: str, is_gce_ingress: bool) -> str:
    print(f"Building ingress config with url: {url}")

    env = Environment(
        loader=FileSystemLoader(os.path.join(output_dir, BASE_K8S_PATH)),
    )
    template = env.get_template("ingress.yaml.template")
    return template.render(url=url, secret=secret, is_gce_ingress=is_gce_ingress)
