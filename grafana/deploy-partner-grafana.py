#!/usr/bin/env python3

import argparse
import subprocess
import os
from config_generator import generate_partner_config
from dashboards import include_dashboard_in_deployment
from kubernetes_configs import create_partner_ingress_config
from shutil import copytree, rmtree
from typing import Any, Dict

DRY_RUN = "dry_run"
DOMAIN_NAME = "domain_name"
OUTPUT_DIR = "output_dir"
ADMIN_PASSWORD = "admin_password"

K8S_PATH = "k8s"
PARTNER_K8S_PATH = "partner"
BASE_K8S_PATH = "base"
CONFIG_PATH = "config"

def main():
    """The main function which will combine all the different steps of running the deployment.
    """
    args = get_args()
    dry_run = args[DRY_RUN]
    domain_name = args[DOMAIN_NAME]
    output_dir = args[OUTPUT_DIR]
    admin_pass = args[ADMIN_PASSWORD]

    if os.path.exists(output_dir):
        print("Old output folder exists. Deleting it: '%s'" % output_dir)
        rmtree(output_dir)

    output_k8s_dir = os.path.join(output_dir, K8S_PATH)
    copytree(k8s_path(), output_k8s_dir)

    build_config(domain_name, output_k8s_dir)
    include_dashboard_in_deployment(output_k8s_dir)
    create_partner_ingress_config(domain_name=domain_name, output_dir=output_k8s_dir)
    set_initial_admin_password(output_dir=output_k8s_dir, admin_password=admin_pass)
    kubernetes_definitions_path = setup_deploy(output_dir=output_k8s_dir, dry_run=dry_run)
    apply_kubernetes_definitions(kubernetes_definitions_path=kubernetes_definitions_path, dry_run=dry_run)


def get_args() -> Dict[str, Any]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """
    parser = argparse.ArgumentParser(description="Deploy the a Grafana instance")

    # store_true says to default to false, but if specified would then turn in to true.
    parser.add_argument('--dry-run',
                        dest="dry_run",
                        action="store_true",
                        help="If true, don't actually deploy the application and just print what would happen on a deployment.")

    parser.add_argument("domain_name",
                        type=str,
                        metavar="DOMAIN_NAME",
                        help="The name of the domain that should resolve to this instance of prometheus. (e.g. grafana.xand.tools)")

    parser.add_argument("output_dir",
                        type=str,
                        metavar="OUTPUT_DIR",
                        help="The output folder where the deployment artifacts will land.")

    parser.add_argument("admin_password",
                        type=str,
                        metavar="ADMIN_PASSWORD",
                        help="The initial admin password. Only sets password on initial installation. It will not change it if grafana has already been deployed")

    args = parser.parse_args()
    return {
        DRY_RUN: args.dry_run,
        DOMAIN_NAME: args.domain_name,
        OUTPUT_DIR: args.output_dir,
        ADMIN_PASSWORD: args.admin_password
    }

def build_config(domain_name: str, output_dir: str):
    """
    Builds a grafana.ini config file


    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    :param domain_name: Internet domain name where grafana instance will be running
    :type domain_name: str
    """
    config_template_path = os.path.abspath(
        os.path.join(os.getcwd(), CONFIG_PATH)
    )
    output_file = os.path.abspath(
        os.path.join(output_dir, PARTNER_K8S_PATH,  "grafana.ini")
    )
    generate_partner_config(domain_name, config_template_path, output_file)

def setup_deploy(output_dir: str, dry_run: bool) -> str:
    """Sets up the deployment by building the kustomize definition using what's in k8s/partner.

    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    :param dry_run: Print what would happen on a deployment rather then deploying if true.
    :type dry_run: bool

    """
    output_path = os.path.abspath(
        os.path.join(output_dir, "grafana.yaml")
    )
    partner_k8s_path = os.path.abspath(
        os.path.join(output_dir, PARTNER_K8S_PATH)
    )

    print(f"Running kustomize at {partner_k8s_path}")
    print(f"Setting up deployment by running kustomize and placing result at {output_path}")

    # This runs kustomize and outputs to grafana.yml
    kustomize_build_result = subprocess.run(["kustomize", "build", "."], cwd=partner_k8s_path, capture_output=True, check=True, encoding="utf-8")
    contents = kustomize_build_result.stdout

    with open(output_path, "w") as f:
        f.write(contents)

    if dry_run:
        print("Kustomize Build output:")
        print(contents)

    return output_path

def k8s_path() -> str:
    return os.path.abspath(
        os.path.join(os.getcwd(), K8S_PATH)
    )

def apply_kubernetes_definitions(kubernetes_definitions_path: str, dry_run: bool):
    """Actually perform the apply of the kubernetes definitions built from kustomize.

    :param kubernetes_definitions_path: The path to the kubernetes definitions file that was generated by kustomize.
    :type kubernetes_definitions_path: str
    """
    print("Running kubernetes apply.")
    cmd = ["kubectl", "apply", "-f", kubernetes_definitions_path]
    if dry_run:
        cmd.append("--dry-run")
    subprocess.run(cmd, check=True, encoding="utf-8")

def set_initial_admin_password(output_dir: str, admin_password: str):
    """Creates the admin-password.env file that kustomize will expect.

    :param output_dir: The output folder where the deployment artifacts will land.
    :type output_dir: str
    :param admin_password: the initial admin password
    :type admin_password: str
    """
    admin_password_file_output = os.path.abspath(
        os.path.join(output_dir, BASE_K8S_PATH, "admin-password.env")
    )

    contents = f"GF_SECURITY_ADMIN_PASSWORD={admin_password}"
    contents += os.linesep

    with open(admin_password_file_output, 'w') as f:
        f.write(contents)

if __name__ == "__main__":
    main()
