import base64
from hvac import Client, exceptions # type: ignore
import os
from typing import List
from urllib.parse import urljoin

class VaultSecrets:
    def __init__(self, vault_cert_path: str):
        # hvac will use environment variables of VAULT_ADDR and VAULT_TOKEN to initialize a client.
        self.client = Client(verify=vault_cert_path)

    def list_vault(self, path: str, filter_deleted=True) -> List[str]:
        """List all the paths under a given path.

        :param path: Specifies the path of the secret to a path where other paths exist. This is specified as part of the URL.
        "type path: str | unicode
        :return: An array of just the key portion of the path
        :rtype: list[str]
        """
        secrets = self.client.secrets.kv.v2.list_secrets(path=path)['data']['keys']

        if filter_deleted:
          secrets = [secret for secret in secrets if not self._is_secret_deleted(path, secret)]

        return secrets

    def _is_secret_deleted(self, path: str, secret: str) -> bool:
        metadata = self.client.secrets.kv.v2.read_secret_metadata(urljoin(path, secret))['data']
        current_version = metadata['current_version']
        current_version_data = metadata['versions'][str(current_version)]
        return current_version_data['destroyed'] or current_version_data['deletion_time']


    def get_all_kv_from_vault(self, path: str) -> dict:
        """Retrieves all keys/values for a given path.

        :param path: Specifies the path of the secret to read. This is specified as part of the URL.
        "type path: str | unicode
        :return: A dictionary with the keys and values at a given path.
        :rtype: dict
        """
        return self.client.secrets.kv.v2.read_secret_version(path=path)['data']['data']

    def get_from_vault(self, path: str, key: str) -> str:
        """Retrieve the secret from vault at the path specified and retrieving the value
        based on the key.

        :param path: Specifies the path of the secret to read. This is specified as part of the URL.
        :type path: str | unicode
        :param key: Specifies the key within the kv pairs located at the path.
        :type key: str | unicode
        :return: The value located at the path and key.
        :rtype: string
        """
        return self.client.secrets.kv.v2.read_secret_version(path=path)['data']['data'][key]

    def get_base64_decode_from_vault(self, path: str, key: str) -> str:
        """Retrieve the secret from vault at the path specified and retrieving the value
        based on the key and assumes the data is base64 encoded and will decode it.

        :param path: Specifies the path of the secret to read. This is specified as part of the URL.
        :type path: str | unicode
        :param key: Specifies the key within the kv pairs located at the path.
        :type key: str | unicode
        :return: The value located at the path and key which is then base64 decoded.
        :rtype: string
        """
        return self._decode_base64(self.get_from_vault(path, key))

    def _decode_base64(self, base64_str: str) -> str:
        return base64.b64decode(bytes(base64_str, 'utf-8')).decode('utf-8')

    def set_to_vault(self, path: str, key: str, value: str):
        """Sets a key value secret to vault at the path specified. It will either patch an exist secret with
        the key and value or will create a new secret with only the key value specified.

        :param path: Specifies the path of the secret to write. This is specified as part of the URL.
        :type path: str | unicode
        :param key: Specifies the key within the kv pairs located at the path.
        :type key: str | unicode
        :param value: Specifies the value to write into vault.
        :type value: str | unicode
        :return: The JSON response of the request.
        :rtype: dict
        """
        try:
            secret = self.client.secrets.kv.v2.read_secret_version(path=path)['data']['data']
            secret.update(dict([(key, value)]))
        except exceptions.InvalidPath:
            # If a secret doesn't already exist then create a new one with just the key value presented.
            secret = dict([(key, value)])

        return self.client.secrets.kv.v2.create_or_update_secret(path=path, secret=secret)

    def set_base64_encode_to_vault(self, path: str, key: str, value: str):
        """Sets a key value secret to vault at the path specified. After base64 encoding the value that will be stored in vault.
        It will either patch an exist secret with the key and value or will create a new secret with only the key value specified.

        :param path: Specifies the path of the secret to write. This is specified as part of the URL.
        :type path: str | unicode
        :param key: Specifies the key within the kv pairs located at the path.
        :type key: str | unicode
        :param value: Specifies the value to write into vault that will be base64 encoded.
        :type value: str | unicode
        :return: The JSON response of the request.
        :rtype: dict
        """
        return self.set_to_vault(path, key, self._encode_base64(value))

    def _encode_base64(self, plain_text: str) -> str:
        return base64.b64encode(bytes(plain_text, 'utf-8')).decode('utf-8')
