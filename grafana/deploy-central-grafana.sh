#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is for deploying a Central Grafana Server.
    The script will deploy a server using `kubectl`.

    Arguments
        VAULT_ADDR (Required) = Address of Vault server
        VAULT_TOKEN (Required) = Token needed to access Vault
        VAULT_CERT_PATH (Required) = Path on local filesystem to find the Root Certificate for the CA.pem file (e.g. /etc/ssl/certs/devCA.pem)
        VAULT_MONITORING_PATH (Required) = Path within Vault to monitoring data, including targets and jwk
        GRAFANA_DOMAIN_NAME (Required) = The url to be able to access the central grafana instance with.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

# No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

# Take arguments
VAULT_ADDR=${1:?"$(error 'VAULT_ADDR must be set' )"}
VAULT_TOKEN=${2:?"$(error 'VAULT_TOKEN must be set' )"}
VAULT_CERT_PATH=${3:?"$(error 'VAULT_CERT_PATH must be set' )"}
VAULT_MONITORING_PATH=${4:?"$(error 'VAULT_MONITORING_PATH must be set' )"}
GRAFANA_DOMAIN_NAME=${5:?"$(error 'GRAFANA_DOMAIN_NAME must be set' )"}


echo "Python version: $(python3 --version)"

pushd ${BASH_SOURCE%/*}/

export PATH=$PATH:$HOME/.local/bin
# Install the pacakges from setup.py
pip3 install -e . --user

export VAULT_ADDR=$VAULT_ADDR
export VAULT_TOKEN=$VAULT_TOKEN
export TEMP_DIR=$(mktemp -d)
./deploy-central-grafana.py $GRAFANA_DOMAIN_NAME $VAULT_MONITORING_PATH $VAULT_CERT_PATH $TEMP_DIR

popd
