#!/usr/bin/env python3

import argparse
import glob
import subprocess
import os
from jinja2 import Environment, FileSystemLoader, select_autoescape
from typing import Any, Dict
from shutil import copyfile, copytree, rmtree

BASE_DIR_NAME = "base"
DASHBOARDS_DIR_NAME = "dashboards"

def include_dashboard_in_deployment(k8s_definitions_dir: str):
    """
    Include dashboards into k8s definitions in order to deploy them.

    :param k8s_definitions_dir: Directory where k8s defintions are expected to be edited.
    :type k8s_definitions_dir: str
    """

    # Source Directories
    dashboard_dir = os.path.abspath(os.path.join(__file__, os.pardir))
    dashboard_files = os.path.abspath(os.path.join(dashboard_dir, "*.json"))

    # Destination Directories
    base_dir = os.path.abspath(os.path.join(k8s_definitions_dir, BASE_DIR_NAME))
    output_dashboard_dir = os.path.join(base_dir, DASHBOARDS_DIR_NAME)
    os.mkdir(output_dashboard_dir)

    files = glob.glob(dashboard_files)
    for file in files:
        filename = os.path.basename(file)
        destination = os.path.join(output_dashboard_dir, filename)
        copyfile(file, destination)

        # We need to use a relative path when using within kustomize since
        # it's going to include the path in the file and the parent directory
        # will be zipped and moved.
        relate_file_path = os.path.join(DASHBOARDS_DIR_NAME, filename)
        subprocess.run(["kustomize",
                        "edit",
                        "add",
                        "configmap",
                        "dashboards",
                        f'--from-file={relate_file_path}'
                        ],
                       cwd=base_dir,
                       check=True,
                       encoding="utf-8")

OUTPUT_DIR = "output_dir"

def cli():
    """
    Tool for building dashboard config from the command line
    """
    args = _get_args()
    output_dir = args[OUTPUT_DIR]
    k8s_dir = os.path.abspath(os.path.join(os.path.abspath(__file__), os.pardir, os.pardir, "k8s"))
    print(f"k8s_dir: {k8s_dir}, output_dir: {output_dir}")
    copytree(k8s_dir, output_dir, dirs_exist_ok=True)
    include_dashboard_in_deployment(output_dir)

def _get_args() -> Dict[str, Any]:
    """
    Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """
    parser = argparse.ArgumentParser(description="Test dashboard inclusion")

    parser.add_argument("output_dir",
                        type=str,
                        metavar="OUTPUT_DIR",
                        help="The output for where we expect the files to go.")

    args = parser.parse_args()
    return {
        OUTPUT_DIR: args.output_dir
    }

if __name__ == '__main__':
    cli()
