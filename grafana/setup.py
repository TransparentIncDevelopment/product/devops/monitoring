from setuptools import setup # type: ignore

setup(name='grafana',
      version='0.1',
      author='engineering@tpfs.io',
      packages=["config_generator", "process", "vault_secrets", "dashboards"],
      install_requires=["hvac", "jinja2"])
