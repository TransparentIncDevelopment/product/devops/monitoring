#!/usr/bin/env bash
# Note: This script is used in CI to conditionally build base docker images by comparing the tag
# specified in the neighboring `.env` file, and the tags published in GCR. If a matching tag already
# exists in GCR, this script will continue without building the image.

# Exit if error occurs
set -e

echo "Running script from: $(pwd)"

# Set /docker path by picking up location of this script
DOCKER_DIR="$( dirname "${BASH_SOURCE[0]}" )"

# Load build env vars and utility functions
source $DOCKER_DIR/.env
source $DOCKER_DIR/util.sh

# monitoring-cd-image (continuous deployment) img ##############################################################
build_and_push_if_image_doesnt_exist $MONITORING_CD_IMG monitoring-cd-image $DOCKER_DIR
