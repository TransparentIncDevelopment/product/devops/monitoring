# Monitoring Repository

This repository contains kubernetes definitions for our monitoring software stack. It includes:
1. node-exporter - A DaemonSet that deploys node-exporter instance on each k8s node. It 
gives metrics on the k8s nodes. https://github.com/prometheus/node_exporter
2. prometheus - A deployment, service and ingress that gathers metrics from 
various targets, like node-exporter, the xand-nodes, and k8s itself. https://github.com/prometheus/prometheus
3. grafana - A deployment, and ingress for a self-hosted grafana website. Provides sophisticated tooling 
for building metrics dashboards. https://github.com/grafana/grafana

This project is licensed under MIT OR Apache-2.0.

# Deployment testing
Each component (node-exporter, grafana, prometheus) requires a different process to deploy, 
but there are make targets to aid the process.

#### Pre-reqs
1. kustomize cli (https://kubectl.docs.kubernetes.io/installation/kustomize/)
1. Minikube installed (https://minikube.sigs.k8s.io/docs/start/)
1. Python3 (https://www.python.org/downloads/)
1. kubectl cli installed and configured for your cluster. (https://kubernetes.io/docs/tasks/tools/)

#### Start a MiniKube cluster

1. Start cluster on your dev machine:
   ```
   make start-cluster
   ```

#### Deploy components

1. Install python deps
   ```
   make install-python-deps
   ```
   
1. Deploy node-exporter
   ```
   make deploy-node-exporter
   ```
   
1. Deploy prometheus
   ```
   make deploy-prometheus
   ```

1. Deploy grafana
   ```
   make deploy-grafana
   ```

1. Observe running pods
   ```
   kubectl get pod -A
   ```

1. To connect to the websites, use kubectl to port-forward:
   ```
   kubectl port-forward -n monitoring <grafana-pod-name> 3000:3000
   kubectl port-forward -n monitoring <prometheus-pod-name> 9090:9090
   kubectl port-forward -n monitoring <node-exporter-pod-name> 9100:9100
   ```
   
   Then open a browser to the corresponding localhost port:
   * Grafana: [http://localhost:3000](http://localhost:3000) (user: `admin`, pass: `1Password!`)
   * Prometheus: [http://localhost:9090](http://localhost:9090)
   * Node-Exporter: [http://localhost:9100/metrics](http://localhost:9100)
   
#### Cleanup
To shutdown and delete your minikube cluster:
```
make delete-cluster
```
