Prometheus Monitoring
=====================

This directory contains k8s definitions for deploying a central Prometheus
server to collect metrics across a given cluster. This will be the default
metrics setup for Partner clusters.

Prometheus expects to be able to reach out to scrape the data it is collecting,
rather than the data source pushing data *to* the prom server.

## Configuration
The server's config is defined in `prometheus_config.yaml`. It scrapes data
by default from services/pods within its own cluster.

### Authorization
As with our other services, JWT based auth is used to protect access to metrics.

JWT generation and storage is slightly different than elsewhere (we could
stop doing it in our Rust code and use Envoy, if we want to stick with JWT) as
Envoy requires a `JWKS` keystore (rather than just a secret string) to validate
tokens. `JWKS` is just a JSON format that describes everything you need to know
about the key.

They can be easily generated here using the provided scripts next to this
README, but the scripts must be run from within the docker container provided
by `xand-devops` (so that dependencies are available).

The `metrics_jwt_setup.sh` script is the entry point.

### Federation of partner deployments
TODO: Describe how federation is configured for partner deployments


## Alerting

The alert configuration for prometheus exists in the file `k8s/base/alert.rules`. An example of what
an alert may look like is the following:

```yaml
    - alert: Deployment at 0 Replicas
      annotations:
        summary: Deployment {{$labels.deployment}} in {{$labels.namespace}} is currently having no pods running
      expr: |
        sum(kube_deployment_status_replicas{pod_template_hash=""}) by (deployment,namespace)  < 1
      for: 1m
      labels:
        team: devops
```

Broken down this starts with a name for the alert `Deployment at 0 Replicas`, a summary that will be included in
the alert which contains labels that are associated with the metric that has caused this alert to go off. In
this case the deployment and the namespace it belongs to. Then the expression that will be evaluated
`sum(kube_deployment_status_replicas{pod_template_hash=""}) by (deployment,namespace) < 1` which says it will
take a look at the `kube_deployment_status_replicas` metric for the deployment and aggregate the sum by it's
deployment and namespace when it's less than 1 it may set off an alert if it's occurs for at least 1
minute. Lastly add the label `team: devops` when the alert does go off in case the additional categorization
says who to alert based on that label.

### Alerting Unit Testing

You'll find our unit tests in the prometheus_alert_tests directory which we'll automatically test using gitlab ci.
The syntax doesn't have a lot of explanation in the documentation so we'll attempt to describe it the best that we
can here. Original Documentation: https://prometheus.io/docs/prometheus/latest/configuration/unit_testing_rules/

In order to test the alert `Deployment at 0 Replicas` above we need to have metric data to test with and so the
first part of the unit test file will describe some mock data and the subsequent part of the unit test file will
describe the unit tests to run against that mock data. First we'll describe some of the more terse to understand
syntax then we'll give a simple example of how this is intended to work.

#### Series Mock Data Values

In the examples you'll see shorthand notation like the following `-2+1x4` for those that can recall algebra
notation this is the equivalent of a linear equation `y = mx + b` where `m = 1` and `b = -2` and this will be
executed 5 times with a starting index of 0. So `-2+1x4` will result in the following long hand values `-2 -1 0
1 2`. To break it down the math looks like the following for each value `(-2 + (1 x 0)) (-2 + (1 x 1)) (-2 + (1
x 2)) (-2 + (1 x 3)) (-2 + (1 x 4))`. So why do we need these values? What we're trying to mock is a metric data
at a particular point in time. So you can look at `-2` as what the metric will look like at t0 and `1` as what
the metric will look like at t3. You must be wondering for what metric and we'll cover that next.

The values above are associated with a metric by including it with a series. An example syntax looks like the
following:

```yaml
      - series: 'kube_deployment_status_replicas{pod_template_hash="",deployment="member-api",namespace="xand"}'
        # 7 1s followed by 7 0s and 3 1s
        values: '1+0x6 0+0x6 1+0x2'
```

The series notation looks like the following: `<metric name>{<label name>=<label value>, ...}` so we have
`kubect_deployment_status_replicas` as the `metric name` and a few different labels associated with this
metric. Then a few value notations to specify the values from t0 to t16. Which we have a comment to help
breakdown what the values will look like.

#### Series Mock Data

```yaml
  - interval: 1m
    # Input mock data that will be used in tests below.
    input_series:
      - series: 'kube_deployment_status_replicas{pod_template_hash="",deployment="member-api",namespace="xand"}'
        # 7 1s followed by 7 0s and 3 1s
        values: '1+0x6 0+0x6 1+0x2'
```

Let's put this all together for the mock data at the moment we don't really have a way to say what the time
looks like other than using arbitrary t0 or t15. Instead let's say that each point in time represents a minute
so we use `interval: 1m` now we have 16 values that happen within 15 minutes each being 1 minute apart.

#### Alert Rule Tests

Now that we have some mock data let's go ahead and write a test given the data we have from above based on the
alert from above. This will use all of the configurations specified in the past couple of sections in this doc.

```yaml
    # Actual tests against mock data above.
    alert_rule_test:
      # At the 10th minute which is the 11th interval we expect member-api to be at 0 as shown above.
      - eval_time: 10m
        alertname: Deployment at 0 Replicas
        exp_alerts:
          - exp_labels:
              team: devops
              deployment: member-api
              namespace: xand
            exp_annotations:
              summary: "Deployment member-api in xand is currently having no pods running"
      - eval_time: 14m
        alertname: Deployment at 0 Replicas
        # No expected alerts
```

So here we have our first test at the 10th minute which is the 11th interval due to starting at the 0th
minute. At that point in time we have 0 for kube_deployment_status_replicas for the member-api deployment. Since
it more than a minute has passed since it's been down which means the alert would go off which is why there's an
expected alert to go off that will have a label of `deployment: member-api`. Hence our expected alert will check
for a summary annotation with the labels filled out and check for labels on the alert.

The next test is at eval_time 14m where the member api deployment is at 1 replicas and so there are no alerts
that should go off by not specifying `exp_alerts` the promtool will make sure no alerts go off.

## Verifying it's working

You can make the prom server accessible locally by port forwarding with k8s.

Make sure you have kubectl set up to interact with the cluster where the prom
server is deployed then:

`kubectl port-forward -n monitoring service/prometheus-service 9090:8080`

Now you can navigate to `http://localhost:9090` to interact with the prom server.
Navigate to the `Status->Targets` page to determine if targets are being
scraped successfully for metrics.


# Federation
Prometheus enables the federation, or aggregation, of many Prometheus metrics servers' data by a
central server. This allows central monitoring of metrics from many sources.

To generate the configuration for this central server, use the `gen_central_prom_server.sh` script:

```bash
sh gen_central_prom_config.sh <input_source_parameters_file> <output_yaml_file>}
```

Use may look like

```
sh gen_central_prom_config.sh sources.txt central_prom_config.yml
```

This script will generate a configuration YAML file that includes the "jobs" for scraping each member of
the federation.

#### Input

The input file is a list of all the sources including the name, target uri, and authorization token in the format:

```
<some_source_name> <some_target_url> <some_auth_token>
```

*Each line is a separate source.* For example, it might look like:

```
xand-dev prometheus-server.dev.xand.tools eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0ZXN0IiwiYXVkIjoidHBmcyIsImlhdCI6MTU5MTE0NDU2NX0.dWS861jprwerjggKyGlKpEtYVFb51qBbSo7afHkMPz_cI
some-member metrics.some.bank.com eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c
some-validator metrics.some.validator.com eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxNjc4OTAiLCJuYW1lIjoiSm8gRG9lIiwiaWF0IjoxNTE2OTAyMn0.hEzHBOVnEGykxSBQaoGnw6-xht8PXA4X76NL6qWXPA4
...
```

#### Output

The script will output a YAML file that can be consumed by Prometheus. You will specify the name of
the `.yml` file as a parameter, and that file will be generated or overwritten.
