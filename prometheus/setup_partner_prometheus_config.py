#!/usr/bin/env python3

import argparse
from config_generator import generate_partner_prometheus_config, generate_partner_envoy_config
from typing import Any, Dict
import os

PARTNER_K8S_PATH = "k8s/partner/"
ISSUER="issuer"

def main():
    args = get_args()

    config_path = os.path.abspath(
        os.path.join(os.getcwd(), "prometheus_config/")
    )
    partner_config_path = os.path.abspath(
        os.path.join(os.getcwd(), PARTNER_K8S_PATH)
    )

    promtheus_config_output = os.path.join(partner_config_path, "prometheus_config.yml")
    generate_partner_prometheus_config(config_path, promtheus_config_output)
    print(f"Generated Prometheus Config to {promtheus_config_output}...")

    envoy_config_output = os.path.join(partner_config_path, "envoy_config.yaml")
    generate_partner_envoy_config(args[ISSUER], config_path, envoy_config_output)
    print(f"Generated Envoy Config to {envoy_config_output}...")

def get_args() -> Dict[str, Any]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """
    parser = argparse.ArgumentParser(description="Setup Configuration for the Partner Prometheus Instance")

    parser.add_argument("issuer",
                        type=str,
                        metavar="ISSUER",
                        help="The issuer that should be specified on the envoy configuration.")

    args = parser.parse_args()
    return {
        ISSUER: args.issuer
    }

if __name__ == '__main__':
    main()
