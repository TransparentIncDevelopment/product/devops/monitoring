from pathlib import Path
import os
from jinja2 import Environment, FileSystemLoader, select_autoescape
from typing import List
from urllib.parse import urljoin
from vault_secrets import VaultSecrets

def generate_central_alertmanager_config(vault_secrets: VaultSecrets, vault_base_path: str, config_path: str, output_file: str):
    smtp = _get_smtp_from_vault(vault_secrets, vault_base_path)
    slack = _get_slack_from_vault(vault_secrets, vault_base_path)
    output = _build_config(smtp, slack, config_path)

    with open(output_file, 'w') as f:
        f.write(output)

def _build_config(smtp: dict, slack: dict, config_path: str) -> str:
    print(f"Building the alertmanager config using templates at {config_path}")
    env = Environment(
        loader=FileSystemLoader(config_path),
        autoescape=select_autoescape(['yml', 'yaml'])
    )
    template = env.get_template("alertmanager_config.yaml.template")
    output = template.render(smtp=smtp, slack=slack)
    return output

def _get_smtp_from_vault(vault_secrets: VaultSecrets, vault_base_path: str) -> dict:
    smtp_path = urljoin(vault_base_path, "smtp")

    return vault_secrets.get_all_kv_from_vault(smtp_path)

def _get_slack_from_vault(vault_secrets: VaultSecrets, vault_base_path: str) -> dict:
    slack_path = urljoin(vault_base_path, "slack")

    return vault_secrets.get_all_kv_from_vault(slack_path)
