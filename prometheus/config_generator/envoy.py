from jinja2 import Environment, FileSystemLoader, select_autoescape
from pathlib import Path

def generate_partner_envoy_config(issuer: str, config_path: str, output_file: str):
    content = _build_config(issuer=issuer, config_path=config_path)
    with open(output_file, 'w') as f:
        f.write(content)

def _build_config(issuer: str, config_path: str) -> str:
    print(f"Building the Envoy config using templates at {config_path}")
    env = Environment(
        loader=FileSystemLoader(config_path),
        autoescape=select_autoescape(['yml', 'yaml'])
    )
    template = env.get_template("envoy_config.yaml.template")
    output = template.render(issuer=issuer)
    return output
