from pathlib import Path
import os
from jinja2 import Environment, FileSystemLoader, select_autoescape
from typing import List
from urllib.parse import urljoin
from vault_secrets import VaultSecrets

def generate_central_prometheus_config(vault_secrets: VaultSecrets, vault_path: str, config_path: str,  output_file: str):
    targets = _get_targets_from_vault(vault_secrets, vault_path)
    output = _build_config(targets, True, config_path)

    with open(output_file, 'w') as f:
        f.write(output)

def generate_partner_prometheus_config(config_path: str,  output_file: str):
    output = _build_config([], False, config_path)

    with open(output_file, 'w') as f:
        f.write(output)

def _build_config(targets: List[dict], include_alerting: bool, config_path: str) -> str:
    print(f"Building the prometheus config using templates at {config_path}")
    env = Environment(
        loader=FileSystemLoader(config_path),
        autoescape=select_autoescape(['yml', 'yaml'])
    )
    template = env.get_template("prometheus_config.yaml.template")
    output = template.render(targets=targets, include_alerting=include_alerting)
    return output

def _get_targets_from_vault(vault_secrets: VaultSecrets, path: str) -> List[dict]:
    target_names = vault_secrets.list_vault(path)
    target_paths = [urljoin(path, name) for name in target_names]
    return [vault_secrets.get_all_kv_from_vault(target_path) for target_path in target_paths]
