#!/usr/bin/env python3

import argparse
from typing import Any, Dict
from urllib.parse import urljoin
from vault_secrets import VaultSecrets

OAUTH_CLIENT_ID="oauth_client_id"
OAUTH_CLIENT_SECRET="oauth_client_secret"
VAULT_CERT_PATH="vault_cert_path"
VAULT_MONITORING_PATH = "vault_monitoring_path"

def main():
    args = get_args()
    vault_secrets = VaultSecrets(args[VAULT_CERT_PATH])
    set_oauth_in_vault(
        vault_secrets=vault_secrets,
        vault_monitoring_path=args[VAULT_MONITORING_PATH],
        oauth_client_id=args[OAUTH_CLIENT_ID],
        oauth_client_secret=args[OAUTH_CLIENT_SECRET]
    )


def get_args() -> Dict[str, Any]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """
    parser = argparse.ArgumentParser(description="Deploy the Central Prometheus Instance")

    parser.add_argument("vault_monitoring_path",
                        type=str,
                        metavar="VAULT_MONITORING_PATH",
                        help="The path in vault to the base directory of monitoring. (e.g. environments/dev/monitoring)")

    parser.add_argument("vault_cert_path",
                        type=str,
                        metavar="VAULT_CERT_PATH",
                        help="The path to the root certificate to be used with vault. (e.g. /etc/ssl/certs/devCA.pem)")

    parser.add_argument('oauth_client_id',
                        type=str,
                        metavar="OAUTH_CLIENT_ID",
                        help="The client id for the oauth credentials that will be used in Google Cloud IAP.")

    parser.add_argument('oauth_client_secret',
                        type=str,
                        metavar="OAUTH_CLIENT_SECRET",
                        help="The client secret for the oauth credentials that will be used in Google Cloud IAP.")

    args = parser.parse_args()
    return {
        VAULT_MONITORING_PATH: args.vault_monitoring_path,
        VAULT_CERT_PATH: args.vault_cert_path,
        OAUTH_CLIENT_ID: args.oauth_client_id,
        OAUTH_CLIENT_SECRET: args.oauth_client_secret
    }

def set_oauth_in_vault(vault_secrets: VaultSecrets, vault_monitoring_path: str, oauth_client_id: str, oauth_client_secret: str):
    path = urljoin(vault_monitoring_path, "oauth")

    vault_secrets.set_to_vault(path, OAUTH_CLIENT_ID, oauth_client_id)
    vault_secrets.set_to_vault(path, OAUTH_CLIENT_SECRET, oauth_client_secret)

if __name__ == '__main__':
    main()
