#!/usr/bin/env python3

import argparse
from config_generator import generate_central_alertmanager_config, generate_central_prometheus_config
import os
import process
import subprocess
import time
from typing import Any, Dict
from vault_secrets import VaultSecrets
from urllib.parse import urljoin

from set_oauth_to_vault import OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET

# Key lookups for arguments from argparse
CENTRAL_K8S_PATH = "k8s/central/"
PROMETHEUS_DOMAIN_NAME = "prometheus_domain_name"
ALERTMANAGER_DOMAIN_NAME = "alertmanager_domain_name"
DRY_RUN = "dry_run"
VAULT_CERT_PATH="vault_cert_path"
VAULT_MONITORING_PATH = "vault_monitoring_path"


def main():
    """The main function which will combine all the different steps of running the deployment.
    """
    args = get_args()

    dry_run = args[DRY_RUN]
    vault_secrets = VaultSecrets(args[VAULT_CERT_PATH])

    generate_prometheus_config(vault_secrets=vault_secrets, vault_base_path=args[VAULT_MONITORING_PATH], dry_run=dry_run)
    generate_alertmanager_config(vault_secrets=vault_secrets, vault_base_path=args[VAULT_MONITORING_PATH], dry_run=dry_run)

    central_path = os.path.join(os.getcwd(), CENTRAL_K8S_PATH)
    replace_ingress_with_domain(path=central_path, template_file_name="central_prometheus.ingress.yaml.template", domain_name=args[PROMETHEUS_DOMAIN_NAME])
    replace_ingress_with_domain(path=central_path, template_file_name="alertmanager.ingress.yaml.template", domain_name=args[ALERTMANAGER_DOMAIN_NAME])
    get_oauth_from_vault(vault_secrets=vault_secrets, vault_base_path=args[VAULT_MONITORING_PATH])
    kubernetes_definitions_path = setup_deploy(dry_run=dry_run)
    validate_kubernetes_def(kubernetes_definitions_path)
    apply_kubernetes_definitions(kubernetes_definitions_path=kubernetes_definitions_path, dry_run=dry_run)

    if not dry_run:
        refresh_config_on_deployment()


def get_args() -> Dict[str, Any]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """
    parser = argparse.ArgumentParser(description="Deploy the Central Prometheus Instance")

    # store_true says to default to false, but if specified would then turn in to true.
    parser.add_argument('--dry-run',
                        dest="dry_run",
                        action="store_true",
                        help="If true, don't actually deploy the application and just print what would happen on a deployment.")

    parser.add_argument("vault_monitoring_path",
                        type=str,
                        metavar="VAULT_MONITORING_PATH",
                        help="The path in vault to the base directory of monitoring. (e.g. environments/dev/monitoring)")

    parser.add_argument("prometheus_domain_name",
                        type=str,
                        metavar="PROMETHEUS_DOMAIN_NAME",
                        help="The name of the domain that should resolve to this instance of prometheus. (e.g. prometheus.xand.tools)")

    parser.add_argument("alertmanager_domain_name",
                        type=str,
                        metavar="ALERTMANAGER_DOMAIN_NAME",
                        help="The name of the domain that should resolve to the instance of alertmanager associated with this prometheus instance. (e.g. alerts.xand.tools)")

    parser.add_argument("vault_cert_path",
                        type=str,
                        metavar="VAULT_CERT_PATH",
                        help="The path to the root certificate to be used with vault. (e.g. /etc/ssl/certs/devCA.pem)")

    args = parser.parse_args()
    return {
        DRY_RUN: args.dry_run,
        VAULT_MONITORING_PATH: args.vault_monitoring_path,
        PROMETHEUS_DOMAIN_NAME: args.prometheus_domain_name,
        ALERTMANAGER_DOMAIN_NAME: args.alertmanager_domain_name,
        VAULT_CERT_PATH: args.vault_cert_path
    }


def generate_prometheus_config(vault_secrets: VaultSecrets, vault_base_path: str, dry_run: bool):
    """Generates the prometheus configuration for the central prometheus server by
    pulling values from vault for the targets.

    :param vault_base_path: The base path within vault where secrets for prometheus can be found.
    :type vault_base_path: str
    """
    print("Generating Prometheus Configuration from Vault")
    vault_targets_path = urljoin(vault_base_path, "targets/")
    config_path = os.path.abspath(
        os.path.join(os.getcwd(), "prometheus_config/")
    )
    output_file = os.path.abspath(
        os.path.join(os.getcwd(), CENTRAL_K8S_PATH,  "prometheus_config.yml")
    )
    generate_central_prometheus_config(
        vault_secrets=vault_secrets,
        vault_path=vault_targets_path,
        config_path=config_path,
        output_file=output_file
    )

    if dry_run:
        with open(output_file) as f:
            print(f.read())


def generate_alertmanager_config(vault_secrets: VaultSecrets, vault_base_path: str, dry_run: bool):
    """Generates the alertmanager configuration for the central prometheus server by
    pulling values from vault for smtp credentials and slack.

    :param vault_secrets: An object that accesses the vault instance which will be used to retrieve secrets from vault.
    :type vault_secrets: VaultSecrets
    :param vault_base_path: The base path within vault where secrets for prometheus (alert manager) can be found.
    :type vault_base_path: str
    :param dry_run: Whether to run this in dry run mode where it prints the contents out to the console.
    :type dry_run: bool
    """

    print("Generating Prometheus' Alert Manager Configuration from Vault")
    config_path = os.path.abspath(
        os.path.join(os.getcwd(), "prometheus_config/")
    )
    output_file = os.path.abspath(
        os.path.join(os.getcwd(), CENTRAL_K8S_PATH,  "alertmanager_config.yml")
    )
    generate_central_alertmanager_config(
        vault_secrets=vault_secrets,
        vault_base_path=vault_base_path,
        config_path=config_path,
        output_file=output_file
    )

    if dry_run:
        with open(output_file) as f:
            print(f.read())


def replace_ingress_with_domain(path: str, template_file_name: str, domain_name: str):
    """Replaces values in the ingress template file and outputs it to a place where
    kustomize will pick it up. Doesn't need to print anything for a dry run since it's included
    in the kustomize output.

    :param path: The path of where the ingress template exists and where the resulting ingress will end up.
    :type path: str
    :param template_file_name: The name of the template file that should end with `.template`.
    :type template_file_name: str
    :param domain_name: The domain name to be used with this deployment of the central server.
    :type domain_name: str
    """
    if not template_file_name.endswith(".template"):
        raise Exception(f"The template file is expected to end with .template and it wasn't found in '${template_file_name}'.")

    secret_name = domain_name.replace(".", "-")
    ingress_template = os.path.abspath(
        os.path.join(path, template_file_name)
    )
    file_name = template_file_name[:-(len(".template"))]
    ingress_output = os.path.abspath(
        os.path.join(path, file_name)
    )
    print(f"Replacing url in {ingress_template} with {domain_name}.")

    with open(ingress_template, "r") as file:
        ingress = file.read()

    # Replacements
    data = ingress \
        .replace("{INSERT_URL_HERE}", domain_name) \
        .replace("{INSERT_SECRET_URL_HERE}", secret_name)

    with open(ingress_output, "w") as file:
        file.write(data)


def get_oauth_from_vault(vault_secrets: VaultSecrets, vault_base_path: str):
    """Creates the prometheus-oauth.env file that kustomize will expect to be there which will hold the values from vault.
    Doesn't need to print anything for a dry run since it's included in the kustomize output.
    """
    path = urljoin(vault_base_path, "oauth")
    oauth_output_path = os.path.abspath(
        os.path.join(os.getcwd(), CENTRAL_K8S_PATH, "prometheus-oauth-client.env")
    )
    print(f"Retrieving oauth from vault at {path} and storing it at: {oauth_output_path}")
    oauth_client_id = vault_secrets.get_from_vault(path, OAUTH_CLIENT_ID)
    oauth_client_secret = vault_secrets.get_from_vault(path, OAUTH_CLIENT_SECRET)

    contents = f"client_id={oauth_client_id}"
    contents += os.linesep
    contents += f"client_secret={oauth_client_secret}"
    contents += os.linesep

    with open(oauth_output_path, 'w') as f:
        f.write(contents)


def setup_deploy(dry_run: bool) -> str:
    """Sets up the deployment by building the kustomize definition using what's in k8s/central.
    """
    k8s_path = os.path.abspath(
        os.path.join(os.getcwd(), CENTRAL_K8S_PATH)
    )
    output_path = os.path.abspath(
        os.path.join(os.getcwd(), "central-monitoring.yaml")
    )

    print(f"Setting up deployment by running kustomize and placing result at {output_path}")

    # This runs kustomize and outputs to central-monitoring.yaml
    kustomize_build_result = subprocess.run(["kustomize", "build", "."], cwd=k8s_path, capture_output=True, check=True, encoding="utf-8")
    contents = kustomize_build_result.stdout

    with open(output_path, "w") as f:
        f.write(contents)

    if dry_run:
        print("Kustomize Build output:")
        print(contents)

    return output_path


def validate_kubernetes_def(path: str):
    subprocess.run(["kubeval", "--ignore-missing-schemas", path], check=True)


def strip_to_just_the_k8s_name(text: str) -> str:
    forward_slash_index = text.find("/") + 1
    name = text[forward_slash_index:]
    return name.strip()


def get_prometheus_deployment_name() -> str:
    """Get the name of the deployment for the central prometheus instance.
    """
    return get_deployment_name('app=prometheus-server')

def get_alertmanager_deployment_name() -> str:
    """Get the name of the deployment for the central alertmanager instance.
    """
    return get_deployment_name('app=alertmanager')

def get_deployment_name(label_filter: str) -> str:
    """Get the name of the deployment in the monitoring namespace.

    :param label_filter: The label filter that's intended to pick the only deployment with that label.
    :type label_filter: str
    """
    result = subprocess.run(
        ["kubectl", "get", "deployment", "-n", "monitoring", "-l", label_filter, "-o", "name"],
        capture_output=True,
        check=True,
        encoding="utf-8"
    )
    return strip_to_just_the_k8s_name(result.stdout)

def get_prometheus_pod_name() -> str:
    """Get the name of the pod for the central prometheus instance.
    """
    return get_pod_name('app=prometheus-server')

def get_alertmanager_pod_name() -> str:
    """Get the name of the pod for the central alertmanager instance.
    """
    return get_pod_name('app=alertmanager')

def get_pod_name(label_filter: str) -> str:
    """Get the name of the pod in the monitoring namespace.

    :param label_filter: The label filter that's intended to pick the only deployment with that label.
    :type label_filter: str
    """
    result = subprocess.run(
        ["kubectl", "get", "pods", "-n", "monitoring", "-l", label_filter, "-o", "name"],
        capture_output=True,
        check=True,
        encoding="utf-8"
    )

    return strip_to_just_the_k8s_name(result.stdout)


def apply_kubernetes_definitions(kubernetes_definitions_path: str, dry_run: bool):
    """Actually perform the apply of the kubernetes definitions built from kustomize.

    :param kubernetes_definitions_path: The path to the kubernetes definitions file that was generated by kustomize.
    :type kubernetes_definitions_path: str
    """
    print("Running kubernetes apply.")
    cmd = ["kubectl", "apply", "-f", kubernetes_definitions_path]
    if dry_run:
        cmd.append("--dry-run")
    subprocess.run(cmd, check=True, encoding="utf-8")


def refresh_config_on_deployment():
    print("Waiting for deployment to succeed...")
    deployment_name = "deployment/" + get_prometheus_deployment_name()
    subprocess.run(["kubectl", "rollout", "status", "-n", "monitoring", "--timeout", "3m", "--watch=true", deployment_name], check=True)

    deployment_name = "deployment/" + get_alertmanager_deployment_name()
    subprocess.run(["kubectl", "rollout", "status", "-n", "monitoring", "--timeout", "3m", "--watch=true", deployment_name], check=True)

    # It can take up to 2 minutes for the configmap to update in the pod:
    # https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/#mounted-configmaps-are-updated-automatically
    # So we're going to wait a minute then attempt a refresh and then do it again on a subsequent minute.
    for x in range(2):
        SLEEP_TIME=60
        print(f"Sleeping for {SLEEP_TIME} seconds to allow config to update")
        time.sleep(SLEEP_TIME)

        pod_name = get_prometheus_pod_name()
        print(f"Sending SIGHUP to server process on ${pod_name} to refresh config...")
        subprocess.check_call([f"kubectl exec {pod_name} -n monitoring -c prometheus -- /bin/sh -c 'kill -HUP 1'"], shell=True)

        pod_name = get_alertmanager_pod_name()
        print(f"Sending SIGHUP to server process on ${pod_name} to refresh config...")
        subprocess.check_call([f"kubectl exec {pod_name} -n monitoring -c alertmanager -- /bin/sh -c 'kill -HUP 1'"], shell=True)

if __name__ == "__main__":
    main()
