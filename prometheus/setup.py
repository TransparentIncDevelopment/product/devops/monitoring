from setuptools import setup # type: ignore

setup(name='monitoring',
      version='0.1',
      author='engineering@tpfs.io',
      packages=["config_generator", "process", "vault_secrets"],
      install_requires=["hvac", "jinja2"])
