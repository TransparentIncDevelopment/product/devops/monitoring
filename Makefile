.PHONY:

# Minikube cluster commands
requires-minikube:
	which minikube > /dev/null || (echo "\n******\nUnable to find Minikube. Please install.\n*******\n"; exit 1)

start-cluster: requires-minikube
	minikube start

delete-cluster: requires-minikube
	minikube delete

requires-cluster:
	@kubectl get nodes > /dev/null || (echo "\n*******\nUnable to connect to your cluster, is it running?\nTo start cluster: make start-cluster\n*******\n"; exit 1)

# Test deployment commands

install-python-deps:
	pip3 install -r prometheus/requirements.txt

requires-kustomize:
	@which kustomize > /dev/null || (echo "\n*******\nUnable to find kustomize cli. Please install.\n*******\n"; exit 1)

deploy-prometheus: requires-cluster requires-kustomize
	touch prometheus/k8s/partner/jwks
	cd prometheus; ./setup_partner_prometheus_config.py fakecompanyinc
	sed -i 's/{INSERT_PROMETHEUS_URL_HERE}/prom.test.xand.to/g' prometheus/k8s/partner/prometheus.ingress.yaml
	sed -i 's/{INSERT_PROMETHEUS_URL_SECRET_HERE}/prom-test-xand-to/g' prometheus/k8s/partner/prometheus.ingress.yaml
	cd	prometheus/k8s/partner; kustomize build . | kubectl apply -f -

deploy-node-exporter: requires-cluster requires-kustomize
	cd node-exporter; kustomize build . | kubectl apply -f -

deploy-grafana: requires-cluster requires-kustomize
	cd grafana; ./deploy-partner-grafana.py grafana.test.xand.to output 1Password!

